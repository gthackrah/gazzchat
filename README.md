# GazzChat #

A demo messaging app, created for Itty Bitty Apps by Gary Thackrah.

### Testing Procedure ###

Run project using GazzChat.xcworkspace.

You'll require multiple devices to test this app. The best option would be to run on multiple instances of a device using the iOS simulator.

To test -

- Run the app and select Create Account
- Enter a name, email and password. You won't need access to the email account, but the address must be in a valid format.
- Repeat the same process on another device / simulator instance
- On device 1, tap the action button (top right hand corner)
- Select the second user you just created
- Type a message and tap send
- You should see the message appear on device 2
- On device 2, open the message and type back
- While you're in the message feed screen, you should see that the other user is typing a message (the title updates)