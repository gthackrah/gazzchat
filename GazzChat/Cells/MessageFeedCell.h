//
//  MessageFeedCell.h
//  GazzChat
//
//  Created by Gary Thackrah on 5/12/17.
//  Copyright © 2017 Gary Thackrah. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MessageLabel.h"

#define kMessageLabelTopMargin                      8
#define kMessageLabelBottomMargin                   20
#define kMessageLabelSideMargin                     20
#define kMessageLabelTextVerticalPadding            10
#define kMessageLabelMaxWidth                       300

@interface MessageFeedCell : UITableViewCell
@property (weak, nonatomic) IBOutlet MessageLabel *messageLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *messageLabelHorizontalConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *messageLabelWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *messageLabelHeightConstraint;
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;

- (void) setupCellWithMessage:(Message *)message withFrameWidth:(CGFloat)width;

@end
