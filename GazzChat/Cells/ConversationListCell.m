//
//  ConversationListCell.m
//  GazzChat
//
//  Created by Gary Thackrah on 5/12/17.
//  Copyright © 2017 Gary Thackrah. All rights reserved.
//

#import "ConversationListCell.h"

@implementation ConversationListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.initialLabel.layer.cornerRadius = self.initialLabel.frame.size.height / 2;
}

- (void) setupCellWithConversation:(Conversation *)conversation {
    if ([[User sharedInstance].uid isEqualToString:conversation.recipient.uid]) {
        self.nameLabel.text = conversation.sender.name;
    }
    else {
        self.nameLabel.text = conversation.recipient.name;
    }
    
    self.messageLabel.text = conversation.mostRecentMessageText;
    
    self.initialLabel.text = [[self.nameLabel.text substringToIndex:1] uppercaseString];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
