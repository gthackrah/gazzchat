//
//  MessageFeedCell.m
//  GazzChat
//
//  Created by Gary Thackrah on 5/12/17.
//  Copyright © 2017 Gary Thackrah. All rights reserved.
//

#import "MessageFeedCell.h"

@implementation MessageFeedCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
}

- (void) setupCellWithMessage:(Message *)message withFrameWidth:(CGFloat)width {
    
    User *user = [User sharedInstance];
    
    self.infoLabel.hidden = YES;

    if (message.isLastInGroup) {
        // Only display the time if this is the last message of a group from the same sender.
        TTTTimeIntervalFormatter *formatter = [[TTTTimeIntervalFormatter alloc] init];
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:message.timestamp];
        NSTimeInterval totalSeconds = [date timeIntervalSinceDate:[NSDate date]];
        self.infoLabel.text = [formatter stringForTimeInterval:totalSeconds];
        self.infoLabel.hidden = NO;
    }

    self.messageLabel.text = message.text;
    
    CGRect textFrame = [message.text boundingRectWithSize:CGSizeMake(kMessageLabelMaxWidth, CGFLOAT_MAX)
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{ NSFontAttributeName:self.messageLabel.font }
                                                  context:nil];
    
    
    self.messageLabelWidthConstraint.constant = textFrame.size.width + kMessageLabelSideMargin;
    self.messageLabelHeightConstraint.constant = textFrame.size.height + kMessageLabelTextVerticalPadding;

    if ([message.sender.uid isEqualToString:user.uid]) {
        // I'm the sender, set the primary colour and place the label on the right hand side.
        self.messageLabelHorizontalConstraint.constant = (width / 2) - (self.messageLabelWidthConstraint.constant / 2) - kMessageLabelSideMargin;
        self.messageLabel.backgroundColor = kPrimaryColor;
        self.infoLabel.textAlignment = NSTextAlignmentRight;
    }
    else {
        // I'm the recipient, set the colour to grey and place the label on the left hand side.
        self.messageLabelHorizontalConstraint.constant = 0 - ((width / 2) - (self.messageLabelWidthConstraint.constant / 2) - kMessageLabelSideMargin);
        self.messageLabel.backgroundColor = [UIColor lightGrayColor];
        self.infoLabel.textAlignment = NSTextAlignmentLeft;

    }
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
