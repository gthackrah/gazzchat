//
//  Conversation.h
//  GazzChat
//
//  Created by Gary Thackrah on 5/12/17.
//  Copyright © 2017 Gary Thackrah. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Conversation : AutoDescriptionObject

/*!
 *  Identifier of the conversation.
 */
@property (nonatomic, strong) NSString *uid;

/*!
 *  A User object representing the recipient
 */
@property (strong, nonatomic) User *recipient;

/*!
 *  A User object representing the sender (instigator of the conversation)
 */
@property (strong, nonatomic) User *sender;

/*!
 *  Most recent message - for easy display in list view
 */
@property (nonatomic, strong) NSString *mostRecentMessageText;

/*!
 *  Hash value of the payload for this conversation.  Used for determining if anything has changed since last load.
 */
@property (nonatomic, strong) NSString *payloadHashValue;

/*!
 *  A BOOL value indicating that the recipient is currently typing, so we should display a notification
 */
@property (nonatomic) BOOL recipientIsTyping;

/*!
 *  A BOOL value indicating that the sender is currently typing, so we should display a notification
 */
@property (nonatomic) BOOL senderIsTyping;

/*!
 *  An array of Message objects for this conversation
 */
@property (nonatomic, strong) NSMutableArray *messages;


+ (Conversation *) conversationWithDict:(NSDictionary *)conversationDict withConversationId:(NSString *) conversationId;

@end
