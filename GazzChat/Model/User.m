//
//  User.m
//  GazzChat
//
//  Created by Gary Thackrah on 5/12/17.
//  Copyright © 2017 Gary Thackrah. All rights reserved.
//

#import "User.h"
#import "Archiver.h"

static User *sharedInstance = nil;

@implementation User

#define kKeyName        @"userInfo"

+ (instancetype)sharedInstance {
    @synchronized(self) {
        if (sharedInstance == nil) {
            sharedInstance = [Archiver retrieve:kKeyName];
            if (sharedInstance == nil) {
                sharedInstance = [[super allocWithZone:NULL] init];
            }
        }
    }
    return sharedInstance;
}

- (id)init {
    self = [super init];
    if (self) {
        [self setConversations:[NSMutableArray array]];
    }
    return self;
}

+ (void)save {
    [Archiver persist:sharedInstance key:kKeyName];
}

@end
