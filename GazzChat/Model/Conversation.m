//
//  Conversation.m
//  GazzChat
//
//  Created by Gary Thackrah on 5/12/17.
//  Copyright © 2017 Gary Thackrah. All rights reserved.
//

#import "Conversation.h"

@implementation Conversation

- (id)init {
    self = [super init];
    if (self) {
        [self setMessages:[NSMutableArray array]];
    }
    return self;
}

+ (Conversation *) conversationWithDict:(NSDictionary *)conversationDict withConversationId:(NSString *) conversationId {
    
    Conversation *conversation = [[Conversation alloc] init];
    conversation.uid = conversationId;
    conversation.payloadHashValue = [Utils hashForValue:conversationDict];
    conversation.mostRecentMessageText = conversationDict[kFieldNameMostRecentMessageText];
    conversation.recipientIsTyping = [conversationDict[kFieldNameRecipientIsTyping] boolValue];
    conversation.senderIsTyping = [conversationDict[kFieldNameSenderIsTyping] boolValue];
    
    User *sender = [[User alloc] init];
    sender.uid = conversationDict[kFieldNameSenderId];
    sender.name = conversationDict[kFieldNameSenderName];
    conversation.sender = sender;
    
    User *recipient = [[User alloc] init];
    recipient.uid = conversationDict[kFieldNameRecipientId];
    recipient.name = conversationDict[kFieldNameRecipientName];
    
    conversation.recipient = recipient;
    
    NSDictionary *messages = conversationDict[kFieldNameMessages];
    for (NSString *key in messages.allKeys) {
        NSDictionary *messageDict = [messages objectForKey:key];
        Message *message = [[Message alloc] init];
        message.uid = key;
        message.text = messageDict[kFieldNameText];
        message.timestamp = [messageDict[kFieldNameTimestamp] longLongValue];
        
        User *recipient = [[User alloc] init];
        recipient.uid = messageDict[kFieldNameRecipientId];
        
        User *sender = [[User alloc] init];
        sender.uid = messageDict[kFieldNameSenderId];
        
        message.recipient = recipient;
        message.sender = sender;
        
        [conversation.messages addObject:message];
        
    }
    
    return conversation;
    
}

@end
