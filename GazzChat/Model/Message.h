//
//  Message.h
//  GazzChat
//
//  Created by Gary Thackrah on 5/12/17.
//  Copyright © 2017 Gary Thackrah. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Message : AutoDescriptionObject

/*!
 *  Identifier of the message.
 */
@property (nonatomic, strong) NSString *uid;

/*!
 *  Actual text of the message
 */
@property (nonatomic, strong) NSString *text;

/*!
 *  A User object representing the sender
 */
@property (strong, nonatomic) User *sender;

/*!
 *  A User object representing the recipient
 */
@property (strong, nonatomic) User *recipient;

/*!
 *  A bool indicating that this is the last in a group of messages from the same sender.  Used to show / hide info label.  Worked out on the fly, not stored.
 */
@property (assign, nonatomic) BOOL isLastInGroup;

/*!
 *  Date of creation. UTC timestamp.
 */
@property (nonatomic, assign) long long timestamp;

@end
