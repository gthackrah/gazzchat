//
//  User.h
//  GazzChat
//
//  Created by Gary Thackrah on 5/12/17.
//  Copyright © 2017 Gary Thackrah. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AutoDescriptionObject.h"

@interface User : AutoDescriptionObject

/*!
 *  Get the shared instance of the class. Singleton pattern.
 *
 *  @return The shared instance of the class.
 */
+ (instancetype)sharedInstance;

/*!
 *  Identifier of the user.
 */
@property (nonatomic, strong) NSString *uid;

/*!
 *  Email address of the user.
 */
@property (nonatomic, strong) NSString *email;

/*!
 *  Name of the user.
 */
@property (nonatomic, strong) NSString *name;

/*!
 *  Bool indicating if user is logged in or not.
 */
@property (nonatomic) BOOL isLoggedIn;

/*!
 *  An array of Conversation objects for this user
 */
@property (nonatomic, strong) NSMutableArray *conversations;

/*!
 *  Save the shared object to the disk.
 */
+ (void)save;

@end
