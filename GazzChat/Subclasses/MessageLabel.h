//
//  MessageLabel.h
//  GazzChat
//
//  Created by Gary Thackrah on 6/12/17.
//  Copyright © 2017 Gary Thackrah. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessageLabel : UILabel

@end
