//
//  Utils.m
//  GazzChat
//
//  Created by Gary Thackrah on 5/12/17.
//  Copyright © 2017 Gary Thackrah. All rights reserved.
//

#import "Utils.h"

@implementation Utils

+ (NSString *) hashForValue:(id)value {
    NSString *resultAsString = [NSString stringWithFormat:@"%@", value];
    return [resultAsString MD5String];
}

@end
