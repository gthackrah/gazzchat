//
//  Constants.h
//  GazzChat
//
//  Created by Gary Thackrah on 5/12/17.
//  Copyright © 2017 Gary Thackrah. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

#define kPrimaryColor [UIColor colorWithHex:0xffFF2D55]

#define kUserSelectSegue        @"UserSelectSegue"
#define kAuthSegue              @"AuthSegue"
#define kMessageFeedSegue       @"MessageFeedSegue"
#define kMessageListSegue       @"MessageListSegue"

#define kFieldNameUsers                     @"users"
#define kFieldNameConversations             @"conversations"
#define kFieldNameMessages                  @"messages"

#define kFieldNameName                      @"name"
#define kFieldNameEmail                     @"email"
#define kFieldNameSenderId                  @"senderId"
#define kFieldNameRecipientId               @"recipientId"
#define kFieldNameText                      @"text"
#define kFieldNameSenderName                @"senderName"
#define kFieldNameRecipientName             @"recipientName"
#define kFieldNameTimestamp                 @"timestamp"
#define kFieldNameMostRecentMessageText     @"mostRecentMessageText"
#define kFieldNameRecipientIsTyping         @"recipientIsTyping"
#define kFieldNameSenderIsTyping            @"senderIsTyping"

#define kAuthActionRegister                     @"register"
#define kAuthActionLogin                        @"login"

#define kCellIdentifierConversationList         @"ConversationListCell"
#define kCellIdentifierMessageFeed              @"MessageFeedCell"


#endif /* Constants_h */
