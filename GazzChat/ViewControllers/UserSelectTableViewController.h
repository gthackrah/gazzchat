//
//  UserSelectTableViewController.h
//  GazzChat
//
//  Created by Gary Thackrah on 5/12/17.
//  Copyright © 2017 Gary Thackrah. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol UserSelectTableViewControllerDelegate <NSObject>
- (void) userSelectTableViewControllerDidSelectUser:(User *)user;
@end

@interface UserSelectTableViewController : UITableViewController

@property (weak, nonatomic) id <UserSelectTableViewControllerDelegate> delegate;

@end
