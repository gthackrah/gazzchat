//
//  MessageFeedViewController.h
//  GazzChat
//
//  Created by Gary Thackrah on 5/12/17.
//  Copyright © 2017 Gary Thackrah. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessageFeedViewController : UIViewController

@property (strong, nonatomic) Conversation *conversation;

@end
