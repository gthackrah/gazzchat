//
//  ConversationsListViewController.m
//  GazzChat
//
//  Created by Gary Thackrah on 5/12/17.
//  Copyright © 2017 Gary Thackrah. All rights reserved.
//

#import "ConversationsListViewController.h"
#import "ConversationListCell.h"
#import "MessageFeedViewController.h"


@interface ConversationsListViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *conversationIds;

@end

@implementation ConversationsListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    self.conversationIds = [[NSMutableArray alloc] init];
    
    self.navigationController.navigationBarHidden = NO;
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCompose target:self action:@selector(onNewConversationTapped:)];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Logout" style:UIBarButtonItemStylePlain target:self action:@selector(onLogoutTapped:)];
    
    [self.tableView registerNib:[UINib nibWithNibName:kCellIdentifierConversationList bundle:nil] forCellReuseIdentifier:kCellIdentifierConversationList];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 90)];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(16, 0, headerView.frame.size.width - 16, headerView.frame.size.height)];
    label.text = @"Messages";
    label.font = [UIFont systemFontOfSize:34 weight:UIFontWeightBold];
    [headerView addSubview:label];

    self.tableView.tableHeaderView = headerView;
    
    [self observeConversationIds];
}

- (void) observeConversationIds {
    
    FIRDatabaseReference *ref = [[FIRDatabase database] reference];

    [[[[ref child:kFieldNameUsers] child:[User sharedInstance].uid] child:kFieldNameConversations] observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        // Gets called whenever the conversation list for this user changes - ie. someone sends us a message.
        
        if (snapshot.value != [NSNull null]) {
            [self.conversationIds removeAllObjects];
            for (NSString *conversationId in snapshot.value) {
                [self.conversationIds addObject:conversationId];
            }
            [self loadConversations];
        }

    }];
}

- (void) loadConversations {
    
    FIRDatabaseReference *ref = [[FIRDatabase database] reference];
    User *user = [User sharedInstance];
    [user.conversations removeAllObjects];
    
    for (NSString *conversationId in self.conversationIds) {
        
        [[[ref child:kFieldNameConversations] child:conversationId] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
            
            NSDictionary *conversationDict = snapshot.value;
            
            [user.conversations addObject:[Conversation conversationWithDict:conversationDict withConversationId:conversationId]];
            
            if (conversationId == [self.conversationIds lastObject]) {
                [self.tableView reloadData];
                [User save];
            }
        }];
    }
    
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)onNewConversationTapped:(id)sender {
    [self performSegueWithIdentifier:kMessageFeedSegue sender:nil];
}

- (IBAction)onLogoutTapped:(id)sender {
    UIAlertController *alertController = [UIAlertController  alertControllerWithTitle:@"Are you sure you want to logout?"  message:nil  preferredStyle:UIAlertControllerStyleActionSheet];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Logout" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        User *user = [User sharedInstance];
        [user.conversations removeAllObjects];
        user.isLoggedIn = NO;
        [User save];
        [self.navigationController popToRootViewControllerAnimated:YES];
        
    }]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
    }]];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [User sharedInstance].conversations.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ConversationListCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifierConversationList];
    
    Conversation *conversation = [[User sharedInstance].conversations objectAtIndex:indexPath.row];
    [cell setupCellWithConversation:conversation];
    
    return cell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    Conversation *conversation = [[User sharedInstance].conversations objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:kMessageFeedSegue sender:conversation];
    
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    if ([segue.identifier isEqualToString:kMessageFeedSegue]) {
        MessageFeedViewController *vc = [segue destinationViewController];
        vc.conversation = sender;
    }
}


@end
