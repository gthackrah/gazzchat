//
//  MessageFeedViewController.m
//  GazzChat
//
//  Created by Gary Thackrah on 5/12/17.
//  Copyright © 2017 Gary Thackrah. All rights reserved.
//

#import "MessageFeedViewController.h"
#import "UserSelectTableViewController.h"
#import "MessageFeedCell.h"

@interface MessageFeedViewController () <UserSelectTableViewControllerDelegate, UITextViewDelegate>
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textViewContainerBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textViewContainerHeightConstraint;
@property (weak, nonatomic) IBOutlet UITextView *messageTextView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) User *recipient;

@property (nonatomic) BOOL isTyping;

@end

@implementation MessageFeedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    if (!self.conversation) {
        [self performSegueWithIdentifier:kUserSelectSegue sender:self];
    }
    else {
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:kFieldNameTimestamp ascending:YES];
        [self.conversation.messages sortUsingDescriptors:@[sortDescriptor]];

        [self loadConversation];
    }
    
    self.messageTextView.layer.cornerRadius = 8;
    self.messageTextView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.messageTextView.layer.borderWidth = 1;
    
    [self.tableView registerNib:[UINib nibWithNibName:kCellIdentifierMessageFeed bundle:nil] forCellReuseIdentifier:kCellIdentifierMessageFeed];

}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if (self.tableView.contentSize.height > self.tableView.frame.size.height) {
        // If there are lots of messages, scroll to the bottom.
        CGPoint offset = CGPointMake(0, self.tableView.contentSize.height - self.tableView.frame.size.height);
        [self.tableView setContentOffset:offset animated:YES];
    }

}

- (void) loadConversation {
    
    self.title = self.conversation.recipient.name;

    User *user = [User sharedInstance];
    
    FIRDatabaseReference *ref = [[FIRDatabase database] reference];
    
    [[[ref child:kFieldNameConversations] child:self.conversation.uid] observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
                
        if (![self.conversation.payloadHashValue isEqualToString:[Utils hashForValue:snapshot.value]]) {
            
            // Only load the conversation if something has changed since the last load.  Use payloadHashValue to check.
            
            NSString *conversationId = self.conversation.uid;
            self.conversation = nil;
            self.conversation = [Conversation conversationWithDict:snapshot.value withConversationId:conversationId];
            
            self.title = self.conversation.recipient.name;

            if ([user.uid isEqualToString:self.conversation.sender.uid]) {
                if (self.conversation.recipientIsTyping) {
                    self.title = [self.conversation.recipient.name stringByAppendingString:@" typing..."];
                }
            }
            if ([user.uid isEqualToString:self.conversation.recipient.uid]) {
                if (self.conversation.senderIsTyping) {
                    self.title = [self.conversation.recipient.name stringByAppendingString:@" typing..."];
                }
            }
            
            NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:kFieldNameTimestamp ascending:YES];
            [self.conversation.messages sortUsingDescriptors:@[sortDescriptor]];

            [self.tableView reloadData];
            
            [self updateConversationOnDisk];
        }
        
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onBackTapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onSendTapped:(id)sender {
    
    if (self.messageTextView.text.length == 0) {
        return;
    }
    
    User *user = [User sharedInstance];
    FIRDatabaseReference *ref = [[FIRDatabase database] reference];
    
    if (!self.conversation) {
        
        // This is a new conversation, so create one.
        
        FIRDatabaseReference *conversationRef = [[ref child:kFieldNameConversations] childByAutoId];

        Conversation *conversation = [[Conversation alloc] init];
        conversation.uid = conversationRef.key;
        conversation.recipient = self.recipient;
        conversation.mostRecentMessageText = self.messageTextView.text;
        
        FIRDatabaseReference *messageRef = [[conversationRef child:kFieldNameMessages] childByAutoId];
        Message *message = [self createMessageWithId:messageRef.key];
        [conversation.messages addObject:message];
        
        [user.conversations addObject:conversation];
        
        NSDictionary *messageDict = @{message.uid: @{kFieldNameSenderId: message.sender.uid,
                                                     kFieldNameRecipientId: self.recipient.uid,
                                                     kFieldNameText: message.text,
                                                     kFieldNameTimestamp: [NSNumber numberWithLongLong:message.timestamp]
                                                     }};
        
        [conversationRef updateChildValues:@{kFieldNameMostRecentMessageText: conversation.mostRecentMessageText,
                                             kFieldNameSenderId: message.sender.uid,
                                             kFieldNameSenderName: message.sender.name,
                                             kFieldNameRecipientId: self.recipient.uid,
                                             kFieldNameRecipientName: self.recipient.name,
                                             kFieldNameMessages: messageDict
                                             }];
        
        
        NSMutableArray *conversationIds = [[NSMutableArray alloc] init];
        for (Conversation *conversation in user.conversations) {
            [conversationIds addObject:conversation.uid];
        }
        [[[ref child:kFieldNameUsers] child:user.uid] updateChildValues:@{kFieldNameConversations: conversationIds}];
        
        [[[[ref child:kFieldNameUsers] child:self.recipient.uid] child:kFieldNameConversations] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
            
            NSMutableArray *recipientConversationIds = [[NSMutableArray alloc] init];

            if (snapshot.value != [NSNull null]) {
                [recipientConversationIds addObjectsFromArray:snapshot.value];
            }
            [recipientConversationIds addObject:conversation.uid];
            [[[ref child:kFieldNameUsers] child:self.recipient.uid] updateChildValues:@{kFieldNameConversations: recipientConversationIds}];
            
        }];
        
        self.conversation = conversation;
        
        [self loadConversation];

    }
    else {
        
        // This is an existing conversation, so just add the message and update some conversation header details.
        
        FIRDatabaseReference *conversationRef = [[ref child:kFieldNameConversations] child:self.conversation.uid];
        FIRDatabaseReference *messageRef = [[conversationRef child:kFieldNameMessages] childByAutoId];

        Message *message = [self createMessageWithId:messageRef.key];

        [self.conversation.messages addObject:message];
        
        NSDictionary *messageDict = @{message.uid: @{kFieldNameSenderId: message.sender.uid,
                                                     kFieldNameRecipientId: self.conversation.recipient.uid,
                                                     kFieldNameText: message.text,
                                                     kFieldNameTimestamp: [NSNumber numberWithLongLong:message.timestamp]
                                                     }};
        
        [[conversationRef child:kFieldNameMessages] updateChildValues:messageDict];
        
        self.conversation.mostRecentMessageText = message.text;
        [conversationRef updateChildValues:@{kFieldNameMostRecentMessageText: self.conversation.mostRecentMessageText,
                                             [self isTypingFieldForUser]: @(NO)
                                             }];

    }
    
    [self.tableView reloadData];
    self.messageTextView.text = @"";
    [self.messageTextView resignFirstResponder];
    self.textViewContainerHeightConstraint.constant = 45;
    self.isTyping = NO;
    
    [self updateConversationOnDisk];

}

- (Message *) createMessageWithId:(NSString *)messageId {
    
    User *user = [User sharedInstance];

    Message *message = [[Message alloc] init];
    message.uid = messageId;
    message.text = self.messageTextView.text;
    User *sender = [[User alloc] init];
    sender.uid = user.uid;
    sender.name = user.name;
    message.sender = sender;
    message.recipient = self.recipient;
    message.timestamp = [[NSDate date] timeIntervalSince1970];
    return message;
}

- (void) updateConversationOnDisk {
    for (Conversation *conversation in [User sharedInstance].conversations) {
        if ([conversation.uid isEqualToString:self.conversation.uid]) {
            conversation.mostRecentMessageText = self.conversation.mostRecentMessageText;
            break;
        }
    }
    [User save];
}

- (NSString *)isTypingFieldForUser {
    
    // Returns a field name for "isTyping", depending on whether we're the sender or recipient.
    if ([self.conversation.recipient.uid isEqualToString:[User sharedInstance].uid]) {
        return kFieldNameRecipientIsTyping;
    }
    else {
        return kFieldNameSenderIsTyping;
    }
    
}

#pragma mark - Keyboard delegates

- (void)keyboardWillShow:(NSNotification *)notif {
    
    NSDictionary *info = [notif userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    [self.view layoutIfNeeded];
    self.textViewContainerBottomConstraint.constant = kbSize.height;
    
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void)keyboardWillHide:(NSNotification *)notif {
    self.textViewContainerBottomConstraint.constant = 0;
}

#pragma mark - UITextViewDelegate

- (void)textViewDidChange:(UITextView *)textView {
    
    // Resize the text view so it will grow and shrink according to its contents.
    
    CGSize sizeThatFitsTextView = [textView sizeThatFits:CGSizeMake(textView.frame.size.width, MAXFLOAT)];
    if (sizeThatFitsTextView.height != textView.frame.size.height) {
        [self.view layoutIfNeeded];
        self.textViewContainerHeightConstraint.constant = sizeThatFitsTextView.height + 12;
        [UIView animateWithDuration:0.3 animations:^{
            [self.view layoutIfNeeded];
        }];
    }
    
    // Tell the server we're typing so the other user can display it.
    
    if (self.conversation) {
        if (!self.isTyping) {
            FIRDatabaseReference *ref = [[FIRDatabase database] reference];
            FIRDatabaseReference *conversationRef = [[ref child:kFieldNameConversations] child:self.conversation.uid];
            
            self.isTyping = YES;
            [conversationRef updateChildValues:@{[self isTypingFieldForUser]: @(YES)}];
        }
    }

    
}

#pragma mark - UserSelectTableViewControllerDelegate

- (void)userSelectTableViewControllerDidSelectUser:(User *)user {
    
    if (!user) {
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    
    self.title = self.conversation.recipient.name;
    self.recipient = user;
    
    
    // Find an existing conversation with the selected user, and load it (if found).
    
    for (Conversation *conversation in [User sharedInstance].conversations) {
        if ([conversation.recipient.uid isEqualToString:user.uid] || [conversation.sender.uid isEqualToString:user.uid]) {
            self.conversation = [[Conversation alloc] init];
            self.conversation.uid = conversation.uid;
            [self loadConversation];
            break;
        }
    }

}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.conversation.messages.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // Resize the row based on the amount of text in the message.  If this is the last message in a group from the same sender, add room for the infoLabel.
    
    Message *message = [self.conversation.messages objectAtIndex:indexPath.row];
    message.isLastInGroup = [self message:message isLastInGroupWithIndex:indexPath.row];
    
    CGRect textFrame = [message.text boundingRectWithSize:CGSizeMake(kMessageLabelMaxWidth, CGFLOAT_MAX)
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{ NSFontAttributeName:[UIFont systemFontOfSize:16 weight:UIFontWeightLight] }
                                                  context:nil];
    
    CGFloat returnVal = textFrame.size.height + kMessageLabelTopMargin + kMessageLabelTextVerticalPadding;
    
    if (message.isLastInGroup) {
        returnVal = returnVal + kMessageLabelBottomMargin;
    }
    
    return returnVal;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MessageFeedCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifierMessageFeed];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    Message *message = [self.conversation.messages objectAtIndex:indexPath.row];
    message.isLastInGroup = [self message:message isLastInGroupWithIndex:indexPath.row];
    
    [cell setupCellWithMessage:message withFrameWidth:tableView.frame.size.width];
    
    return cell;
}

- (BOOL) message:(Message *)message isLastInGroupWithIndex:(NSInteger)index {
    
    if (index + 1 == self.conversation.messages.count) {
        return YES;
    }
    
    Message *nextMessage = [self.conversation.messages objectAtIndex:index + 1];
    if ([nextMessage.sender.uid isEqualToString:message.sender.uid]) {
        return NO;
    }
    
    return YES;
    
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    if ([segue.identifier isEqualToString:kUserSelectSegue]) {
        UINavigationController *nav = segue.destinationViewController;
        UserSelectTableViewController *vc = [nav.viewControllers objectAtIndex:0];
        vc.delegate = self;
    }
    
}


@end
