//
//  UserSelectTableViewController.m
//  GazzChat
//
//  Created by Gary Thackrah on 5/12/17.
//  Copyright © 2017 Gary Thackrah. All rights reserved.
//

#import "UserSelectTableViewController.h"

@interface UserSelectTableViewController ()

@property (strong, nonatomic) NSMutableArray *users;

@end

@implementation UserSelectTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    self.title = @"Select Recipient";
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(onCancelTapped:)];
    
    self.users = [[NSMutableArray alloc] init];
    
    FIRDatabaseReference *ref = [[FIRDatabase database] reference];
    
    [[ref child:kFieldNameUsers] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        
        NSDictionary *dict = snapshot.value;
        for (NSString *key in dict.allKeys) {
            NSDictionary *userDict = [dict objectForKey:key];
            
            if (![key isEqualToString:[User sharedInstance].uid]) {
                // Don't show myself
                User *user = [[User alloc] init];
                user.uid = key;
                user.name = userDict[kFieldNameName];
                [self.users addObject:user];
            }

        }
        
        [self.tableView reloadData];
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onCancelTapped:(id)sender {
    [self.delegate userSelectTableViewControllerDidSelectUser:nil];
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.users.count;
}

static NSString *cellIdentifier = @"MyIdentifier";

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
    cell.textLabel.font = [UIFont systemFontOfSize:18 weight:UIFontWeightLight];
    
    User *user = [self.users objectAtIndex:indexPath.row];
    cell.textLabel.text = user.name;
    
    return cell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    User *user = [self.users objectAtIndex:indexPath.row];
    [self.delegate userSelectTableViewControllerDidSelectUser:user];
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];

}


@end
