//
//  AuthViewController.m
//  GazzChat
//
//  Created by Gary Thackrah on 5/12/17.
//  Copyright © 2017 Gary Thackrah. All rights reserved.
//

#import "AuthViewController.h"


@interface AuthViewController () <KeyboardAccessoryViewDelegate>

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollViewHeightConstraint;

@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIButton *actionButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nameContainerHeightConstraint;

@property (strong, nonatomic) KeyboardAccessoryView *keyboardAccessoryView;

@property (strong, nonatomic) BLMultiColorLoader *multiColorLoader;
@property (weak, nonatomic) IBOutlet UILabel *headingLabel;

@end

@implementation AuthViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.actionButton.layer.cornerRadius = self.actionButton.frame.size.height / 2;

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    self.keyboardAccessoryView = [[KeyboardAccessoryView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    self.keyboardAccessoryView.keyboardAccessoryViewDelegate = self;
    
    self.nameTextField.inputAccessoryView = self.keyboardAccessoryView;
    self.emailTextField.inputAccessoryView = self.keyboardAccessoryView;
    self.passwordTextField.inputAccessoryView = self.keyboardAccessoryView;
    
    if ([self.action isEqualToString:kAuthActionLogin]) {
        self.headingLabel.text = @"Log In";
        [self.actionButton setTitle:@"Log In" forState:UIControlStateNormal];
        self.nameContainerHeightConstraint.constant = 0;
        [self.emailTextField becomeFirstResponder];
    }
    else {
        [self.nameTextField becomeFirstResponder];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if (textField == self.nameTextField)
        [self.emailTextField becomeFirstResponder];
    
    if (textField == self.emailTextField)
        [self.passwordTextField becomeFirstResponder];
    
    if (textField == self.passwordTextField) {
        [textField resignFirstResponder];
        [self onActionButtonTapped:nil];
    }
    
    return NO;
}

#pragma mark - Keyboard delegates

- (void)keyboardWillShow: (NSNotification *)notif {
    
    NSDictionary *info = [notif userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    self.scrollViewHeightConstraint.constant = self.view.frame.size.height - kbSize.height - 40;
    
    [self.view layoutIfNeeded];
    
}

- (void)keyboardWillHide: (NSNotification *)notif {
    
    self.scrollViewHeightConstraint.constant = self.view.frame.size.height;
}

- (IBAction)onActionButtonTapped:(id)sender {
    
    if ([self.action isEqualToString:kAuthActionLogin]) {
        [self login];
    }
    else {
        [self createAccount];
    }
    
}

- (void) login {
    
    NSString *email = self.emailTextField.text;
    NSString *password = self.passwordTextField.text;
    
    [self startAnimation];

    [[FIRAuth auth] signInWithEmail:email password:password completion:^(FIRUser * _Nullable user, NSError * _Nullable error) {
        
        if (user) {
            
            FIRDatabaseReference *ref = [[FIRDatabase database] reference];

            [[[ref child:kFieldNameUsers] child:user.uid] observeSingleEventOfType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {

                if (snapshot.value != [NSNull null]) {
                    User *localUser = [User sharedInstance];
                    localUser.uid = user.uid;
                    localUser.name = snapshot.value[kFieldNameName];
                    localUser.email = snapshot.value[kFieldNameEmail];
                    localUser.isLoggedIn = YES;
                    [User save];
                    
                    [self performSegueWithIdentifier:kMessageListSegue sender:self];
                }
                else {
                    [self showAlertWithTitle:@"Error" withMessage:@"Error getting user"];
                }
                [self stopAnimation];
                
            }];
            
        }
        else {
            [self showAlertWithTitle:@"Error" withMessage:error.localizedDescription];
            [self stopAnimation];
        }
    }];
}

- (void) createAccount {
    
    NSString *name = self.nameTextField.text;
    NSString *email = self.emailTextField.text;
    NSString *password = self.passwordTextField.text;
    
    if (name.length == 0) {
        [self showAlertWithTitle:@"Error" withMessage:@"Please enter a name"];
        [self.nameTextField becomeFirstResponder];
        return;
    }
    
    if (![email isValidEmail]) {
        [self showAlertWithTitle:@"Error" withMessage:@"Please enter a valid email address"];
        [self.emailTextField becomeFirstResponder];
        return;
    }
    
    if (password.length < 6) {
        [self showAlertWithTitle:@"Error" withMessage:@"Password must be at least 6 characters"];
        [self.passwordTextField becomeFirstResponder];
        return;
    }
    
    [self startAnimation];
    
    [[FIRAuth auth] createUserWithEmail:email password:password completion:^(FIRUser * _Nullable user, NSError * _Nullable error) {
        
        if (user) {
            
            User *localUser = [User sharedInstance];
            localUser.uid = user.uid;
            localUser.name = name;
            localUser.email = email;
            localUser.isLoggedIn = YES;
            [User save];
            
            FIRDatabaseReference *ref = [[FIRDatabase database] reference];
            
            [[[ref child:@"users"] child:localUser.uid] setValue:@{@"email": email, @"name": name}];
            
            [self performSegueWithIdentifier:kMessageListSegue sender:self];
        }
        else {
            [self showAlertWithTitle:@"Error" withMessage:error.localizedDescription];
        }
        
        [self stopAnimation];
        
    }];
}

- (IBAction)onBackTapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onBackgroundTapped:(id)sender {
    if (self.nameTextField.isFirstResponder) { [self.nameTextField resignFirstResponder]; }
    if (self.emailTextField.isFirstResponder) { [self.emailTextField resignFirstResponder]; }
    if (self.passwordTextField.isFirstResponder) { [self.passwordTextField resignFirstResponder]; }
}

- (void) startAnimation {
    self.multiColorLoader = [[BLMultiColorLoader alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
    self.multiColorLoader.center = CGPointMake(self.view.frame.size.width / 2, (self.view.frame.size.height / 2) - 50);
    
    self.multiColorLoader.lineWidth = 2.0;
    self.multiColorLoader.colorArray = [NSArray arrayWithObjects:[UIColor whiteColor], nil];
    [self.multiColorLoader startAnimation];
    [self.view addSubview:self.multiColorLoader];
    
    self.actionButton.enabled = NO;
}

- (void) stopAnimation {
    [self.multiColorLoader removeFromSuperview];
    self.multiColorLoader = nil;
    self.actionButton.enabled = YES;
}

#pragma mark - KeyboardAccessoryViewDelegate

- (void)keyboardAccessoryViewDidSelectDone {
    [self onBackgroundTapped:nil];
}

- (void)showAlertWithTitle:(NSString *)title withMessage:(NSString *)message {
    
    UIAlertController *alertController = [UIAlertController  alertControllerWithTitle:title message:message  preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
    }]];
    
    [self presentViewController:alertController animated:YES completion:nil];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
