//
//  LoginMenuViewController.m
//  GazzChat
//
//  Created by Gary Thackrah on 5/12/17.
//  Copyright © 2017 Gary Thackrah. All rights reserved.
//

#import "LoginMenuViewController.h"
#import "AuthViewController.h"

@interface LoginMenuViewController ()

@property (weak, nonatomic) IBOutlet UIButton *createAccountButton;
@property (weak, nonatomic) IBOutlet UIButton *logInButton;

@end

@implementation LoginMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.createAccountButton.layer.cornerRadius = self.createAccountButton.frame.size.height / 2;
    self.logInButton.layer.cornerRadius = self.createAccountButton.layer.cornerRadius;
    self.logInButton.layer.borderWidth = 1;
    self.logInButton.layer.borderColor = [UIColor whiteColor].CGColor;
    
    if ([User sharedInstance].isLoggedIn) {
        [self performSegueWithIdentifier:kMessageListSegue sender:self];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onCreateAccountTapped:(id)sender {
    [self performSegueWithIdentifier:kAuthSegue sender:kAuthActionRegister];
}

- (IBAction)onLoginTapped:(id)sender {
    [self performSegueWithIdentifier:kAuthSegue sender:kAuthActionLogin];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:kAuthSegue]) {
        AuthViewController *vc = [segue destinationViewController];
        vc.action = sender;
    }
    
}


@end
