//
//  AuthViewController.h
//  GazzChat
//
//  Created by Gary Thackrah on 5/12/17.
//  Copyright © 2017 Gary Thackrah. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AuthViewController : UIViewController

@property (strong, nonatomic) NSString *action;

@end
