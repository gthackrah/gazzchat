//
//  NSString+Email.h
//  GazzChat
//
//  Created by Gary Thackrah on 5/12/17.
//  Copyright © 2017 Gary Thackrah. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Email)

- (BOOL) isValidEmail;

@end
