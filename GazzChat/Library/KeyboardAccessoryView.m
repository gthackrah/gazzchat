//
//  KeyboardAccessoryView.m
//  GazzChat
//
//  Created by Gary Thackrah on 5/12/17.
//  Copyright © 2017 Gary Thackrah. All rights reserved.
//

#import "KeyboardAccessoryView.h"

@implementation KeyboardAccessoryView

- (id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if ( self )
    {
        
        
        UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        
        UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(onDoneTapped:)];
        
        doneButton.tintColor = kPrimaryColor;
        NSArray *barArray = [NSArray arrayWithObjects:flexibleSpace, doneButton, nil];
        
        [self setItems:barArray];
        
    }
    return self;
    
}

- (IBAction)onDoneTapped:(UIBarButtonItem *)sender {
    [self.keyboardAccessoryViewDelegate keyboardAccessoryViewDidSelectDone];
}

@end
