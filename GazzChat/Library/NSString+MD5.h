//
//  NSString+MD5.h
//  HelloInspections
//
//  Created by Gary Thackrah on 13/7/17.
//  Copyright © 2017 Gary Thackrah. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (MD5)
- (NSString *)MD5String;
@end
