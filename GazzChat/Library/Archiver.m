//
//  Archiver.m
//  OpenStack
//
//  Created by Mike Mayo on 10/4/10.
//  The OpenStack project is provided under the Apache 2.0 license.
//

#import "Archiver.h"

@implementation Archiver

+ (id)retrieve:(NSString *)key {
    NSString *archivePath = [Archiver archivePath];
    NSString *filePath = [archivePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.archive", key]];
    return [NSKeyedUnarchiver unarchiveObjectWithFile:filePath];
}

+ (BOOL)persist:(id)object key:(NSString *)key {
    if (!object) {
        return NO;
    }
    NSString *archivePath = [Archiver archivePath];
    NSString *filePath = [archivePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.archive", key]];
    return [NSKeyedArchiver archiveRootObject:object toFile:filePath];
}

+ (BOOL)delete:(NSString *)key {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *archivePath = [Archiver archivePath];
    NSString *filePath = [archivePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.archive", key]];
    return [fileManager removeItemAtPath:filePath error:NULL];
}

+ (BOOL)deleteFilesByDate:(NSDate *)beforeDate forExtentions:(NSSet *)extensions {
    BOOL result = YES;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *archivePath = [Archiver archivePath];
    
    NSArray *files = [fileManager contentsOfDirectoryAtPath:archivePath error:NULL];
    
    for (int i = 0; i < [files count]; i++) {
        NSString *path = [archivePath stringByAppendingPathComponent:[files objectAtIndex:i]];
        NSDictionary* attrs = [fileManager attributesOfItemAtPath:path error:nil];
        NSDate *modificationDate = [attrs objectForKey: NSFileModificationDate];
        if ((!beforeDate || [modificationDate compare:beforeDate] == NSOrderedAscending) && (!extensions || [extensions containsObject:[[path pathExtension] lowercaseString]])) {
            result = result && [fileManager removeItemAtPath:path error:NULL];
        }
    }
    
    return result;
}

+ (BOOL)exist:(NSString *)key {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *archivePath = [Archiver archivePath];
    NSString *filePath = [archivePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.archive", key]];
    return [fileManager fileExistsAtPath:filePath];
}

+ (NSString *)archivePath {
#ifdef ARCHIVETOCACHEPATH
    return [Archiver cachePath];
#else
    return [Archiver documentPath];
#endif
}

+ (NSString *)documentPath {
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}

+ (NSString *)cachePath {
    return [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
}

@end
