//
//  AutoDescriptionObject.m
//  GazzChat
//
//  Created by Gary Thackrah on 5/12/17.
//  Copyright © 2017 Gary Thackrah. All rights reserved.
//

#import "AutoDescriptionObject.h"
#import "NSObject+AutoCoding.h" // Need AutoCoding Category

@implementation AutoDescriptionObject
- (NSString *)description {
    NSMutableString *string = [[NSMutableString alloc] initWithString:@"{\r"];
    for (NSString *key in [self codableKeys])
    {
        [string appendFormat:@"%@ : %@,\r", key, [self valueForKey:key]];
    }
    [string appendString:@"}\r"];
    return string;
}
@end
