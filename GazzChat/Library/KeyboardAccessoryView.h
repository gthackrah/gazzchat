//
//  KeyboardAccessoryView.h
//  GazzChat
//
//  Created by Gary Thackrah on 5/12/17.
//  Copyright © 2017 Gary Thackrah. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol KeyboardAccessoryViewDelegate <NSObject>
@required
- (void) keyboardAccessoryViewDidSelectDone;
@end

@interface KeyboardAccessoryView : UIToolbar

@property (assign) id <KeyboardAccessoryViewDelegate> keyboardAccessoryViewDelegate;

- (id)initWithFrame:(CGRect)frame;

@end
