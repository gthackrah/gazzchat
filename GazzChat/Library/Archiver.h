//
//  Archiver.h
//  OpenStack
//
//  Created by Mike Mayo on 10/4/10.
//  The OpenStack project is provided under the Apache 2.0 license.
//

#import <Foundation/Foundation.h>

#define ARCHIVETOCACHEPATH

@interface Archiver : NSObject {

}

+ (id)retrieve:(NSString *)key;
+ (BOOL)persist:(id)object key:(NSString *)key;
+ (BOOL)delete:(NSString *)key;

// Delete the archives whose modification date is earlier than beforeDate and file extension is in extensions.
// If beforeDate is nil, delete files on any date.
// If extensions is nil, delete files with any extension.
// Extensions parameter must be lowercase, case insensitive.
+ (BOOL)deleteFilesByDate:(NSDate *)beforeDate forExtentions:(NSSet *)extensions;
+ (BOOL)exist:(NSString *)key;
+ (NSString *)archivePath;
+ (NSString *)documentPath;
+ (NSString *)cachePath;
@end
