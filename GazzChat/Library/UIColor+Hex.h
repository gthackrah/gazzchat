//
//  UIColor+Hex.h
//  GazzChat
//
//  Created by Gary Thackrah on 5/12/17.
//  Copyright © 2017 Gary Thackrah. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Hex)

+ (UIColor *)colorWithHex:(uint)hex;
+ (UIColor *)colorWithHexString:(NSString *)hexString;
@end
